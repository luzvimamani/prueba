const {store}= require('./store')
 
export const httpClient = {
    get:(ruta) => fetch(ruta,{
        method:'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': store.getToken()
        }        
    })
    .then(r => r.json()),
    post:(ruta, datos) => fetch(ruta,{
        method:'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': store.getToken()
        },body:JSON.stringify(datos)        
    })
    .then(r => r.json()),
    del:(ruta) => fetch(ruta,{
        method:'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': store.getToken()
        }        
    })
    .then(r => r.json()),
    patch:(ruta, datos) => fetch(ruta,{
        method:'PATCH',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': store.getToken()
        },body:JSON.stringify(datos)
    })
    .then(r => r.json()),
}
export const httpClientMonitor = {
    get:(ruta) => fetch(ruta,{
        method:'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': store.get('monitor')
        }        
    })
    .then(r => r.json()),
    post:(ruta, datos) => fetch(ruta,{
        method:'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': store.get('monitor')
        },body:JSON.stringify(datos)
    })
    .then(r => r.json()),
    del:(ruta) => fetch(ruta,{
        method:'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': store.getToken()
        }        
    })
    .then(r => r.json()),
    patch:(ruta, datos) => fetch(ruta,{
        method:'PATCH',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': store.getToken()
        },body:JSON.stringify(datos)
    })
    .then(r => r.json()),
}