const prefix = "SIPOC_DNP_"

export const store = {
    save:(item, data) => {
        localStorage.setItem(prefix+item, JSON.stringify(data))
    },
    get:(item) => {
        let k = localStorage.getItem(prefix+item)
        return k?JSON.parse(k):null
    },
    delete:(item)=> {
        localStorage.removeItem(prefix+item)
    },
    saveToken:(data) => {
        if(data)
            localStorage.setItem(prefix+'TK',data)
    },
    getToken:() => {
        let k  = localStorage.getItem(prefix+'TK')
        return k?k:null
    },
    deleteToken:()=>{
        localStorage.removeItem(prefix+'TK')
        localStorage.removeItem(prefix+'user')
    }
}
