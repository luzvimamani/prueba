import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    usuario:null
  },
  getters: {
  },
  mutations: {
    loginlogout(state){
      setTimeout(() => {
        if(localStorage.getItem('TRANSPARENCY_DNP_user')) state.usuario = JSON.parse(localStorage.getItem('TRANSPARENCY_DNP_user'))
        else state.usuario = null
      }, 1000); 
    }
  },
  actions: {
  },
  modules: {
  }
})

