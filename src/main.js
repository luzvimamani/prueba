import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import Vuex from 'vuex'
import Store from './store'
import Toasted from 'vue-toasted'
import * as VueGoogleMaps from 'vue2-google-maps';

Vue.use(VueGoogleMaps, {
  load: {
      key: 'AIzaSyDp3Ud3SixagW8bJOwtosy5bBMB5JRNS_k',
      libraries:'places',
  }}
)

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(require('vue-moment'))
Vue.use(Toasted,{
  duration:4000,
  keepOnHover:true,
})

new Vue({
  vuetify,
  router,
  store:Store,
  render: h => h(App)
}).$mount('#app')
