export const RUTA_UIF = "http://localhost:3000/";
//export const RUTA_UIF = "https://pdpac.dirnoplu.gob.bo/api/"
export const RUTA_CD = "https://cuenta.demo.gtic.gob.bo/"
export const CLIENT_ID = "SfOfd-YJ3P-QJThK7jeRq"
export const CLIENT_SECRET = "3UedRWghQNfTUziGHCL9ca_KeS-B4ziRWWg_35q-FP6KA3GKLNBuB5EGYvjECDNUEBAyAHHqS-P-qp5xnCh3Vw"
export const REDIRECT_URI = "https://olimpo-test.dirnoplu.gob.bo/statics/oauth/login.html"
export const REDIRECT_LOGOUT_URI = "https://olimpo-test.dirnoplu.gob.bo/statics/oauth/logout.html"




const {store} = require('./store')
const manejarRespuesta = (respuesta) => {
  //console.log(respuesta)
    // Manejar respuesta como se necesite. Yo lo hago así:
  //Actualizando token
  if(respuesta.tk) store.saveToken(respuesta.saveToken)
  if (respuesta.status != 200) {
    return respuesta.msg
  }
  return respuesta.data;
};
export const httpCliente = {
  postfirma: (ruta, datos) => fetch(ruta, {
    method: 'POST',
    body: JSON.stringify(datos),
    headers: {
        'Content-Type': 'application/json',
        'Authorization': store.getToken()
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
  })
    .then(r => r.json())
    .then(r => {return r}),


    post: (ruta, datos) => fetch(ruta, {
        method: 'POST',
        body: JSON.stringify(datos),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': store.getToken()
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
      })
        .then(r => r.json())
        .then(manejarRespuesta),
    get: (ruta) => fetch(ruta, {
          method: 'GET',
          headers: {
              'Content-Type': 'application/json',
              'Authorization': store.getToken()
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
        })
          .then(r => r.json())
          .then(manejarRespuesta),
    patch: (ruta, datos) => fetch(ruta, {
            method: 'PATCH',
            body: JSON.stringify(datos),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': store.getToken()
                // 'Content-Type': 'application/x-www-form-urlencoded',
              },
          })
            .then(r => r.json())
            .then(manejarRespuesta),
    postCiudadania:(datos) => fetch(RUTA_CD+'token', {
      method: 'POST',
      body: datos,
      headers: {
          //'Content-Type': 'application/json',          
          'Content-Type': 'application/x-www-form-urlencoded',
        },
    })
      .then(r => r.json()),
    postCiudadaniaIntro:(datos) => fetch(RUTA_CD+'me', {
      method: 'POST',
      body: datos,
      headers: {
          //'Content-Type': 'application/json',          s
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Bearer ' + store.getToken()
        },
    })
      .then(r => r.json())      
}

